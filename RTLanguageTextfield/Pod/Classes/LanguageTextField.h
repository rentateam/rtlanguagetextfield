//
//  LanguageTextField.h
//
//  Created by A-25 on 27/10/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageTextField : UITextField

@property(nonatomic,strong) NSString *userDefinedKeyboardLanguage;

@end
