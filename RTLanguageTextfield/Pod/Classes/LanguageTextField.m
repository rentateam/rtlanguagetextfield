//
//  LanguageTextField.m
//
//  Created by A-25 on 27/10/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import "LanguageTextField.h"

@implementation LanguageTextField

+ (NSString *)langFromLocale:(NSString *)locale
{
    NSRange r = [locale rangeOfString:@"_"];
    if (r.length == 0){
        r.location = locale.length;
    }
    NSRange r2 = [locale rangeOfString:@"-"];
    if (r2.length == 0){
        r2.location = locale.length;
    }
    return [[locale substringToIndex:MIN(r.location, r2.location)] lowercaseString];
}

- (UITextInputMode *)textInputMode
{
    for (UITextInputMode *tim in [UITextInputMode activeInputModes]) {
        if ([[LanguageTextField langFromLocale:self.userDefinedKeyboardLanguage] isEqualToString:[LanguageTextField langFromLocale:tim.primaryLanguage]]){
            return tim;
        }
    }
    return [super textInputMode];
}

@end
