# RTLanguageTextfield

[![CI Status](http://img.shields.io/travis/a-25/RTLanguageTextfield.svg?style=flat)](https://travis-ci.org/a-25/RTLanguageTextfield)
[![Version](https://img.shields.io/cocoapods/v/RTLanguageTextfield.svg?style=flat)](http://cocoapods.org/pods/RTLanguageTextfield)
[![License](https://img.shields.io/cocoapods/l/RTLanguageTextfield.svg?style=flat)](http://cocoapods.org/pods/RTLanguageTextfield)
[![Platform](https://img.shields.io/cocoapods/p/RTLanguageTextfield.svg?style=flat)](http://cocoapods.org/pods/RTLanguageTextfield)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RTLanguageTextfield is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RTLanguageTextfield"
```

## Author

a-25, anuryadov@gmail.com

## License

RTLanguageTextfield is available under the MIT license. See the LICENSE file for more info.
