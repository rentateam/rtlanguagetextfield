#
# Be sure to run `pod lib lint RTLanguageTextfield.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "RTLanguageTextfield"
  s.version          = "1.0.1"
  s.summary          = "TextField with setting keyboard language."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
Subclass of UITextfield that allows you to set the keyboard default language.
                       DESC

  s.homepage         = "https://bitbucket.org/rentateam/rtlanguagetextfield"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "a-25" => "a-25@rentateam.ru" }
  s.source           = { :git => "https://bitbucket.org/rentateam/rtlanguagetextfield", :tag => s.version }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'RTLanguageTextfield/Pod/Classes/**/*.{h,m}'
  s.resource_bundles = {
    'RTLanguageTextfield' => ['RTLanguageTextfield/Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
